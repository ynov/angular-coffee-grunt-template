copyBower = (distDir, src, vendorDest) ->
    expand: true
    cwd: 'bower_components/' + distDir
    src: src
    dest: 'vendor/' + vendorDest

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    copy:
      bootstrap: copyBower('bootstrap/dist', ['**'], '')
      jquery: copyBower('jquery/dist', ['**'], 'js/')
      angular: copyBower('angular', ['angular.*', 'angular-csp.css'], 'js/')
      angularRoute: copyBower('angular-route', ['angular-route.*'], 'js/')

    coffee:
      compile:
        files:
          'scripts/all.js': ['scripts/coffee/*.coffee']

    watch:
      coffeescripts:
        files: ['scripts/coffee/*.coffee']
        tasks: ['coffee']
        options:
          spawn: false

  grunt.loadNpmTasks 'grunt-contrib-copy'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.registerTask 'default', ['coffee']

  return
