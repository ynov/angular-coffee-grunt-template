todoApp = angular.module 'todoApp', []

todoApp.controller 'TodoListController', () ->
  todoList = this

  todoList.todos = [
    { text: 'learn angular', done: true },
    { text: 'build an angular app', done: false },
  ]

  todoList.addTodo = () ->
    todoList.todos.push { text: todoList.todoText, done: false }
    todoList.todoText = ''
    return

  todoList.remaining = () ->
    count = 0
    angular.forEach todoList.todos, (todo) ->
      count += todo.done ? 0 : 1
      return
    count

  todoList.archive = () ->
    oldTodos = todoList.todos
    todoList.todos = []
    angular.forEach oldTodos, (todo) ->
      todoList.todos.push todo if !todo.done
      return
    return

  return
